> Notre objectif : Permettre à l’Etat de Genève de situer ses pratiques en matière de Gouvernance numérique dans un contexte international. 
Nous examinerons les actions et politiques de l’Etat vis-à-vis des technologies en constante évolution, telles que l’intelligence artificielle, les algorithmes, 
les données ouvertes, les plateformes numériques mais aussi la désinformation, l’accès à l’information, la transparence, la discrimination et 
la protection de la vie privée, dans des domaines spécifiques. 

* [Notre projet de recherche participative 2021-2022](https://2022.wikipolitics.ch) - En cours de réalisation
* [Notre projet de recherche participative 2020-2021](https://2021.wikipolitics.ch)
* [Notre projet de recherche participative 2019-2020](https://2020.wikipolitics.ch)
